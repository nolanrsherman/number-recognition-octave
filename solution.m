% // Programming Assingment 2, Machine Learning II
% // Author: Nolan Sherman
% // Code Inspired / copied from Dr. T. Hoque

% We are solving the single digit classification problem.

% Output Y Encoding is assumed as:
%================================
% hotshot of length 10 where 1.0 at an index corresponds
% to the predicted integer [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]

% Initialize random hidden layers and layer configurations
Numbers = randi(95, 16, 1)+5
Layers = {
    [4 Numbers(1) 10]
    [4 Numbers(2) Numbers(3) Numbers(4) 10]
    [4 Numbers(5) Numbers(6) Numbers(7) Numbers(8) Numbers(9) 10] 
    [4 Numbers(10) Numbers(11) Numbers(12) Numbers(13) Numbers(14) Numbers(15) Numbers(16) 10]
}

% Initialize Constants
alpha = 0.2;   % //usually alpha < 0, ranging from 0.1 to 1
target_mse=0.0001 % // one of the exit condition
Max_Epoch=100  % // one of the exit condition


% ////////// load datasets
%TRAINING DATA
X=load("X.txt")      % // contains features: Column1: x1 (sum) Column2 : x2 (width) Column3: x3 (height) Column4: x4 (Center Weight)
[Nx,P]=size(X); % // Nx = # of sample in X, P= # of feature in X
Y=load("Y.txt")      % // Target Output
[Ny,K]=size(Y); % // Ny = # of target output in Y, K= # of class for K classes when K>=3 otherwise, K=2 in this binary case now

% Optional: Since input and output are kept in different files, it is better to verify the loaded sample size/dimensions.
if Nx ~= Ny 
    error ('The input/output sample sizes do not match');
end

%TESTING DATA
Xtest=load("X_test.txt")
[Nxtest,Ptest]=size(Xtest); % // Nx = # of sample in X, P= # of feature in X
Ytest=load("Y_test.txt")
[Nytest,Ktest]=size(Ytest); % // Ny = # of target output in Y, K= # of class for K classes when K>=3 otherwise, K=2 in this binary case now

%Check that input column width matches.
if P ~= Ptest 
    error ('The number of input columns in test sample does not match the training sample');
end

%Check that output column width matches
if K ~= Ktest 
    error ('The number of output columns in test sample does not match the training sample');
end

% // ------  BEGIN MAIN LOOP --------
% // Run Layer Configurations 1-4
% // ---------------------------------
for i=1:length(Layers)
    
    L = cell2mat(Layers(i)); % Assign this Run's Layer configuration
    epoch=0;       % // 1 epoch => One forward and backward sweep of the net for each training sample 
    mse =Inf;      % // initializing the Mean Squared Error with a very large value.
    Min_Error=Inf
    Min_Error_Epoch=-1
    Err=[];
    Epo=[];

    % Optional
    if L(1) ~= P
        error ('The number of input nodes must be equal to the size of the features')' 
    end 

    % Optional
    if L(end) ~= K
        error ('The number of output node should be equal to K')' 
    end 

    % ---- Initialize Terms ------- %
    % -- Beta
    B=cell(length(L)-1,1);  % forming the number of Beta/weight matrix needed in between the layers

    for i=1:length(L)-1        % Assign uniform random values in [-0.7, 0.7] 
        B{i} =[1.4.*rand(L(i)+1,L(i+1))-0.7];	
    end 

    % -- T
    T=cell(length(L),1); %Let us allocate places for Term, T
    for i=1:length(L)
        T{i} =ones (L(i),1);
    end

    % -- Activation, Z
    Z=cell(length(L),1); %Let us allocate places for activation, i.e., Z
    for i=1:length(L)-1
        Z{i} =zeros (L(i)+1,1); % it does not matter how do we initialize (with '0' or '1', or whatever,) this is fine!
    end
    Z{end} =zeros (L(end),1);  % at the final layer there is no Bias unit

    % -- Delta, d
    d=cell(length(L),1); % Let us allocate places for error term delta, d
    for i=1:length(L)
        d{i} =zeros (L(i),1);
    end
    % ----END Initializing Terms-------
    % ---------------------------------


    % --- BEGIN Training Epoch Loop -----
    while ((mse > target_mse) && (epoch < Max_Epoch))  

        CSqErr=0; 		

        for j=1:Nx 		   
            Z{1} = [X(j,:) 1]';  
            Yk   = Y(j,:)'; 	   
        
            % // forward propagation 
            for i=1:length(L)-1
                
                T{i+1} = B{i}' * Z{i};
                if (i+1)<length(L)
                    Z{i+1}=[(1./(1+exp(-T{i+1}))) ;1];
                else  
                    Z{i+1}=(1./(1+exp(-T{i+1}))); 
                end 
            end  
            % // End forward Propagation
      
                   
            % // Calculate Squared Error
            CSqErr= CSqErr+sum(sum(((Yk-Z{end}).^2),1));   
            CSqErr = CSqErr/L(end);  % Normalizing the Error based on the number of output Nodes
      
            % // Compute error term delta 'd' for each of the node except the input unit
            % // -----------------------------------------------------------------------
            d{end}=(Z{end}-Yk).*Z{end}.*(1-Z{end}); % // delta error term for the output layer

            for i=length(L)-1:-1:2 
                d{i}=Z{i}(1:end-1).*(1-Z{i}(1:end-1)).*sum((d{i+1}'.*B{i}(1:end-1,:)),2); % //sum(A,2) => row wise sum                                                                 
            end

            % // Now we will update the parameters/weights
            for i=1:length(L)-1 
                B{i}(1:end-1,:)=B{i}(1:end-1,:)-alpha.*(Z{i}(1:end-1)*d{i+1}');  
                B{i}(end,:)=B{i}(end,:)-alpha.*d{i+1}';  			
            end              
             
        end  % //end of for loop #1

        CSqErr= (CSqErr) /(Nx);        % //Average error of N sample after an epoch 
        mse=CSqErr 
        epoch  = epoch+1
        
        Err = [Err mse];
        Epo = [Epo epoch];   


        if mse < Min_Error
        Min_Error=mse;
            Min_Error_Epoch=epoch;
        end
    
    end
    % -- End Training Epoch Loop ------
    % ---------------------------------

    %plot epoch versus error graph
    plot (Epo,Err)  % plot based on full epoch
    
end 
% // ------  END MAIN LOOP -----------
% // ---------------------------------