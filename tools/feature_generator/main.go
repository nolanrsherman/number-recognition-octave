/**
* This package is an executable tool for generating a input X.txt and output Y.txt
* From the provided handwritten number data.
 */
package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	//Declare output structures
	X := make([][]float64, 0, 100)
	Y := make([]float64, 0, 100)

	//Get filname from args
	inputFile := flag.String("input", "", "The hand written number data, test or training.")
	disableEngineering := flag.Bool("disable-engineer", false, "disables the engineering feature of this program. program will output raw values.")
	xFile := "X.txt"
	yFile := "Y.txt"

	flag.Parse()

	if *inputFile == "" {
		log.Fatal("Input file is a required flag.")
	}

	//Open File
	file, err := os.Open(*inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	//Read file
	scanner := bufio.NewScanner(file)
	for scanner.Scan() { //Parse Line
		line := strings.TrimSpace(scanner.Text())
		raw := strings.Split(line, " ")
		//parse the raw string line to float slice
		parsed := make([]float64, 0, 1+(16*16))
		for i, s := range raw {
			f, err := strconv.ParseFloat(s, 64)
			if err != nil {
				log.Fatal("Failed to parse element at: ", i)
			}
			if i != 0 { //if not the output column
				f++ //add 1, we want are x's to be positive, and -1 is the lowest from raw data
			}
			parsed = append(parsed, f)
		}
		//append values to appropriated slice
		X = append(X, parsed[1:])
		Y = append(Y, parsed[0])
	}

	//handle error
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	if !*disableEngineering {
		X = engineerFeatures(X)
	}

	Yhotshot := convertToHotShot(Y)

	//Write Y file
	write2dArray(Yhotshot, yFile)
	//Write X file
	write2dArray(X, xFile)
}

func convertToHotShot(Y []float64) [][]float64 {

	hotshot := make([][]float64, 0, len(Y))
	for _, num := range Y {
		output := make([]float64, 10, 10)
		output[int(num)] = 1.0
		hotshot = append(hotshot, output)
	}
	return hotshot

}

func engineerFeatures(X [][]float64) [][]float64 {
	//----------------------
	// Feature Engineer X
	//
	//3 features: Sum, width/height, center weighted density
	//----------------------
	maxDensitySum := maxDensitySum()
	xPrime := make([][]float64, 0, len(X)) //features
	for _, bitmap := range X {
		features := make([]float64, 3) //features for input i

		//Sum
		sum := float64(0)
		for _, pixel := range bitmap {
			sum += pixel
		}
		features[0] = sum / 512 //512 is the max posible, divide for normalization

		//Width - count the spaces on left and right for each row.
		//subtract from 16 to get width of row. update with larger width.
		width := float64(0)
		for i := 0; i < 16; i++ {
			space := 0
			//scan L->R
			for j := 0; j < 16; j++ {
				pixel := bitmap[(i*16)+j]
				if pixel == 0 {
					space++
				} else {
					break
				}
			}
			//scan R->L
			for j := 15; j >= 0; j-- {
				pixel := bitmap[(i*16)+j]
				if pixel == 0 {
					space++
				} else {
					break
				}
			}
			cWidth := float64(16 - space)
			if cWidth > width {
				width = cWidth
			}
		}

		//Height - count the spaces on Top and bottom for each column.
		//subtract from 16 to get height of col. update height with larger height.
		height := float64(0)
		for i := 0; i < 16; i++ {
			space := 0
			//scan T->B
			for j := 0; j < 16; j++ {
				pixel := bitmap[i+(j*16)]
				if pixel == 0 {
					space++
				} else {
					break
				}
			}
			//scan B->T
			for j := 15; j >= 0; j-- {
				pixel := bitmap[i+(j*16)]
				if pixel == 0 {
					space++
				} else {
					break
				}
			}
			cHeight := float64(16 - space)
			if cHeight > height {
				height = cHeight
			}
		}
		features[1] = width / height

		//Weighted Center Density
		density := float64(0)
		for i := 0; i < 16; i++ {
			for j := 0; j < 16; j++ {
				pixel := bitmap[(i*16)+j] - 1          //want to include magnitude of negative values.
				densityB := desnityMatrix[i][j]        //density multipier for matrix position
				density += pixel * densityB * densityB //multiply by densityMult^2 to exagerate
				//the weight that the center of number has.
			}
		}
		features[2] = density / maxDensitySum
		xPrime = append(xPrime, features) //store features in X
	}
	//--END FEATURE ENGINEER --
	return xPrime
}

func maxDensitySum() float64 {
	//Weighted Center Density
	bitmap := make([]float64, 16*16)

	density := float64(0)
	for i := 0; i < 16; i++ {
		for j := 0; j < 16; j++ {
			pixel := bitmap[(i*16)+j] - 1          //want to include magnitude of negative values.
			densityB := desnityMatrix[i][j]        //density multipier for matrix position
			density += pixel * densityB * densityB //multiply by densityMult^2 to exagerate
			//the weight that the center of number has.
		}
	}
	return density
}

func write2dArray(X [][]float64, filename string) {
	xf, err := os.Create(filename)
	if err != nil {
		log.Fatal("Could not create y file: ", err)
	}
	defer xf.Close()
	for _, line := range X {
		for i, v := range line {
			if i > 0 {
				xf.WriteString(" ")
			}
			xf.WriteString(fmt.Sprintf("%.4f", v))
		}
		xf.WriteString("\n")
	}
}

func writeY(Y []float64, filename string) {
	yf, err := os.Create(filename)
	if err != nil {
		log.Fatal("Could not create y file: ", err)
	}
	defer yf.Close()
	for _, v := range Y {
		yf.WriteString(fmt.Sprintf("%.4f\n", v))
	}
}

var desnityMatrix = [][]float64{
	[]float64{0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 5 / 7.0, 6 / 7.0, 6 / 7.0, 6 / 7.0, 6 / 7.0, 5 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 5 / 7.0, 6 / 7.0, 7 / 7.0, 7 / 7.0, 6 / 7.0, 5 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 5 / 7.0, 6 / 7.0, 7 / 7.0, 7 / 7.0, 6 / 7.0, 5 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 5 / 7.0, 6 / 7.0, 6 / 7.0, 6 / 7.0, 6 / 7.0, 5 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 5 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 4 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 3 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 2 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 1 / 7.0, 0 / 7.0},
	[]float64{0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0, 0 / 7.0},
}
