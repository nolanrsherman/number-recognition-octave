# README

## Run ANN
To run the individual ANNs, first open up this project directory in MATLAB or Octave. Then open the ANN you wish to run of the format solution_ann_<#>.md. Then Run that file. If you wish to decrease the amount of epochs, the file partial_init_vars.m can be edited. 

## Run Feature Generator
The source code for the Feature generator is available in ./tools/feature_generator. A windows executable of this generator is available at ./tools/feature_generator.exe. It requires one flag, input. usage is like so:
```./tools/feature_generator.exe -input text.txt```  
This will export a X.txt and a Y.txt file to the directory that program was initiated in.