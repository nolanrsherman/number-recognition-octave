% ////////// load datasets
%TRAINING DATA
X=load("X.txt")      % // contains features: Column1: x1 (sum) Column2 : x2 (width) Column3: x3 (height) Column4: x4 (Center Weight)
[Nx,P]=size(X); % // Nx = # of sample in X, P= # of feature in X
Y=load("Y.txt")      % // Target Output
[Ny,K]=size(Y); % // Ny = # of target output in Y, K= # of class for K classes when K>=3 otherwise, K=2 in this binary case now

% Optional: Since input and output are kept in different files, it is better to verify the loaded sample size/dimensions.
if Nx ~= Ny 
    error ('The input/output sample sizes do not match');
end

%TESTING DATA
Xtest=load("X_test.txt")
[Nxtest,Ptest]=size(Xtest); % // Nx = # of sample in X, P= # of feature in X
Ytest=load("Y_test.txt")
[Nytest,Ktest]=size(Ytest); % // Ny = # of target output in Y, K= # of class for K classes when K>=3 otherwise, K=2 in this binary case now

%Check that input column width matches.
if P ~= Ptest 
    error ('The number of input columns in test sample does not match the training sample');
end

%Check that output column width matches
if K ~= Ktest 
    error ('The number of output columns in test sample does not match the training sample');
end

% Optional
if L(1) ~= P
    error ('The number of input nodes must be equal to the size of the features')' 
end 

% Optional
if L(end) ~= K
    error ('The number of output node should be equal to K')' 
end 