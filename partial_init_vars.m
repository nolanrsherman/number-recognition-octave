% Initialize Global vars
alpha = 0.4;   % //usually alpha < 0, ranging from 0.1 to 1
target_mse=0.00000000001 % // one of the exit condition
Max_Epoch=2000  % // one of the exit condition
Min_Error=Inf
Min_Error_Epoch=-1
epoch=0;       % // 1 epoch => One forward and backward sweep of the net for each training sample 
mse =Inf;      % // initializing the Mean Squared Error with a very large value.
Err=[];
Epo=[];
testErr=[];
Accuracy=[];