
% ---- Initialize Terms ------- %
% -- Beta
B=cell(length(L)-1,1);  % forming the number of Beta/weight matrix needed in between the layers

for i=1:length(L)-1        % Assign uniform random values in [-0.7, 0.7] 
    B{i} =[1.4.*rand(L(i)+1,L(i+1))-0.7];	
end 

% -- T
T=cell(length(L),1); %Let us allocate places for Term, T
for i=1:length(L)
    T{i} =ones (L(i),1);
end

% -- Activation, Z
Z=cell(length(L),1); %Let us allocate places for activation, i.e., Z
for i=1:length(L)-1
    Z{i} =zeros (L(i)+1,1); % it does not matter how do we initialize (with '0' or '1', or whatever,) this is fine!
end
Z{end} =zeros (L(end),1);  % at the final layer there is no Bias unit

% -- Delta, d
d=cell(length(L),1); % Let us allocate places for error term delta, d
for i=1:length(L)
    d{i} =zeros (L(i),1);
end
% ----END Initializing Terms-------
% ---------------------------------


% --- BEGIN Training Epoch Loop -----
while ((mse > target_mse) && (epoch < Max_Epoch))  

    CSqErr=0; 		

    for j=1:Nx 		   
        Z{1} = [X(j,:) 1]';  
        Yk   = Y(j,:)'; 	   
    
        % // forward propagation 
        for i=1:length(L)-1
            
            T{i+1} = B{i}' * Z{i};
            if (i+1)<length(L)
                Z{i+1}=[(1./(1+exp(-T{i+1}))) ;1];
            else  
                Z{i+1}=(1./(1+exp(-T{i+1}))); 
            end 
        end  
        % // End forward Propagation
    
                
        % // Calculate Squared Error
        CSqErr= CSqErr+sum(sum(((Yk-Z{end}).^2),1));   
        CSqErr = CSqErr/L(end);  % Normalizing the Error based on the number of output Nodes
    
        % // Compute error term delta 'd' for each of the node except the input unit
        % // -----------------------------------------------------------------------
        d{end}=(Z{end}-Yk).*Z{end}.*(1-Z{end}); % // delta error term for the output layer

        for i=length(L)-1:-1:2 
            d{i}=Z{i}(1:end-1).*(1-Z{i}(1:end-1)).*sum((d{i+1}'.*B{i}(1:end-1,:)),2); % //sum(A,2) => row wise sum                                                                 
        end

        % // Now we will update the parameters/weights
        for i=1:length(L)-1 
            B{i}(1:end-1,:)=B{i}(1:end-1,:)-alpha.*(Z{i}(1:end-1)*d{i+1}');  
            B{i}(end,:)=B{i}(end,:)-alpha.*d{i+1}';  			
        end              
            
    end  % //end of for loop #1


    CSqErr= (CSqErr) /(Nx);        % //Average error of N sample after an epoch 
    mse=CSqErr 
    Err = [Err mse];
    
    testCSqErr = 0;
    numCorrect = 0;
    % ----- Run Test --------
    for j=1:Nxtest
        Z{1} = [Xtest(j,:) 1]';   % Load Inputs with bias=1
        Yk   = Ytest(j,:)'; 	   
        % // forward propagation 
        for i=1:length(L)-1
            
            T{i+1} = B{i}' * Z{i};
            if (i+1)<length(L)
                Z{i+1}=[(1./(1+exp(-T{i+1}))) ;1];
            else  
                Z{i+1}=(1./(1+exp(-T{i+1}))); 
            end 
        end  
        % // End forward Propagation 
            
        % // Calculate Squared Error
        testCSqErr= testCSqErr+sum(sum(((Yk-Z{end}).^2),1));   
        testCSqErr = testCSqErr/L(end);  % Normalizing the Error based on the number of output Nodes
        [input, expected]= max(Ytest(j,:)');
        [output, result] = max(Z{end});
        if expected==result
            numCorrect=numCorrect+1;
        end
    end 
    testAccuracy = numCorrect/Nxtest
    Accuracy = [Accuracy testAccuracy];

    testCSqErr= (testCSqErr) /(Nxtest);        % //Average error of N sample after an epoch 
    testMse=testCSqErr
    testErr = [testErr testMse];

    epoch  = epoch+1
    Epo = [Epo epoch];   


    if mse < Min_Error
    Min_Error=mse;
        Min_Error_Epoch=epoch;
    end

end
% -- End Training Epoch Loop ------
% ---------------------------------

%plot epoch versus error graph
hold off
plot (Epo,Err)  % plot based on full epoch (Blue Line)
hold on
plot(Epo, testErr) % Red Line
hold off
figure

minErr= min(Err);
minTestErr = min(testErr);
fileID = fopen(FILENAME,'w');
fprintf(fileID,'Min Training MSE: %e\n', minErr);
fprintf(fileID,'Min Test MSE: %e\n',minTestErr);
fclose(fileID);
plot(Epo, Accuracy) %Figure 2 Blue Line
