% // Programming Assingment 2, Machine Learning II
% // Author: Nolan Sherman
% // Code Inspired / copied from Dr. T. Hoque

% We are solving the single digit classification problem.

% Output Y Encoding is assumed as:
%================================
% hotshot of length 10 where 1.0 at an index corresponds
% to the predicted integer [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]

% Initialize random hidden layers and layer configurations
L=[3 20 40 50 40 20 10]
FILENAME='Ann_5.result.txt';
% Init Vars
partial_init_vars

% Load Data
partial_load_data

% Run ANN
partial_run

