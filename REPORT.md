# Fall 2019: CSCI 4588 Programming Assignment #3
#### Nolan Sherman

## Overview
In this assignment I leveraged 4 Neural Networks with different configurations to solve a 10 class classification problem. The Raw input provided was a data set containing 256 grascale quantities representing a 16x16 image and the digit that they represented. 

## Data Transformation
I first wrote a Go program to parse this raw data and generate two file: Y.txt which contains a list of the expected outputs, And X.txt which contained a transformation of the original 256 value input into 3 feature input. The transformed input contains 3 columns with the following generated values ordered respectiely: Sum of all grayscale values normalized, Ratio of width to height, and again a sum of grayscale values with the center of the image weighted with 1, the edges 0, and a even gradient in between.


## Results
#### ANN Configurations
|NAME|Input|Hidden Layer|Output|
|---|---|---|---|
|ANN #1|3|50|10|
|ANN #3|3|20 50 20|10|
|ANN #5|3|20 40 50 40 20|10|
|ANN #7|3|40 50 70 30 70 50 40|10|

#### Results per ANN after 2000 epochs

|ANN|Min Train MSE|Min Test MSE|Max Test Accuracy|
|---|---|---|---|
|#1|1.595248e-07 |1.313072e-07|52.32%|
|#3|7.685463e-08|1.802047e-08|53.01%|
|#5|1.004442e-07|1.127779e-08|53.86%|
|#7|6.700316e-08|2.537299e-08|54.86%|

## Result Line Graphs
Key: 
- Left: Blue=Train MSE, Red=Test MSE
- Right: Blue=Test Accuracy

#### ANN #1
![ANN #1](ann_1.graph.png)
#### ANN #3
![ANN #3](ann_3.graph.png)
  
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

#### ANN #5
![ANN #5](ann_5.graph.png)
#### ANN #7
![ANN #7](ann_7.graph.png)

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

## Code Sample
```m
% .... Initialize Constants, Collections for error, and the ANN configuration
% ---- Initialize Terms ------- %
% -- Beta
B=cell(length(L)-1,1);  % forming the number of Beta/weight matrix needed in between the layers

for i=1:length(L)-1        % Assign uniform random values in [-0.7, 0.7] 
    B{i} =[1.4.*rand(L(i)+1,L(i+1))-0.7];	
end 

% -- T
T=cell(length(L),1); %Let us allocate places for Term, T
for i=1:length(L)
    T{i} =ones (L(i),1);
end

% -- Activation, Z
Z=cell(length(L),1); %Let us allocate places for activation, i.e., Z
for i=1:length(L)-1
    Z{i} =zeros (L(i)+1,1); % it does not matter how do we initialize (with `0` or `1`, or whatever,) this is fine!
end
Z{end} =zeros (L(end),1);  % at the final layer there is no Bias unit

% -- Delta, d
d=cell(length(L),1); % Let us allocate places for error term delta, d
for i=1:length(L)
    d{i} =zeros (L(i),1);
end
% ----END Initializing Terms-------
% ---------------------------------


% --- BEGIN Training Epoch Loop -----
while ((mse > target_mse) && (epoch < Max_Epoch))  

    CSqErr=0; 		

    for j=1:Nx 		   
        Z{1} = [X(j,:) 1]`;  
        Yk   = Y(j,:)`; 	   
    
        % // forward propagation 
        for i=1:length(L)-1
            
            T{i+1} = B{i}` * Z{i};
            if (i+1)<length(L)
                Z{i+1}=[(1./(1+exp(-T{i+1}))) ;1];
            else  
                Z{i+1}=(1./(1+exp(-T{i+1}))); 
            end 
        end  
        % // End forward Propagation
    
                
        % // Calculate Squared Error
        CSqErr= CSqErr+sum(sum(((Yk-Z{end}).^2),1));   
        CSqErr = CSqErr/L(end);  % Normalizing the Error based on the number of output Nodes
    
        % // Compute error term delta `d` for each of the node except the input unit
        % // -----------------------------------------------------------------------
        d{end}=(Z{end}-Yk).*Z{end}.*(1-Z{end}); % // delta error term for the output layer

        for i=length(L)-1:-1:2 
            d{i}=Z{i}(1:end-1).*(1-Z{i}(1:end-1)).*sum((d{i+1}`.*B{i}(1:end-1,:)),2); % //sum(A,2) => row wise sum                                                                 
        end

        % // Now we will update the parameters/weights
        for i=1:length(L)-1 
            B{i}(1:end-1,:)=B{i}(1:end-1,:)-alpha.*(Z{i}(1:end-1)*d{i+1}`);  
            B{i}(end,:)=B{i}(end,:)-alpha.*d{i+1}`;  			
        end              
            
    end  % //end of for loop #1


    CSqErr= (CSqErr) /(Nx);        % //Average error of N sample after an epoch 
    mse=CSqErr 
    Err = [Err mse];
    
    testCSqErr = 0;
    numCorrect = 0;
    % ----- Run Test --------
    for j=1:Nxtest
        Z{1} = [Xtest(j,:) 1]`;   % Load Inputs with bias=1
        Yk   = Ytest(j,:)`; 	   
        % // forward propagation 
        for i=1:length(L)-1
            
            T{i+1} = B{i}` * Z{i};
            if (i+1)<length(L)
                Z{i+1}=[(1./(1+exp(-T{i+1}))) ;1];
            else  
                Z{i+1}=(1./(1+exp(-T{i+1}))); 
            end 
        end  
        % // End forward Propagation 
            
        % // Calculate Squared Error
        testCSqErr= testCSqErr+sum(sum(((Yk-Z{end}).^2),1));   
        testCSqErr = testCSqErr/L(end);  % Normalizing the Error based on the number of output Nodes
        [input, expected]= max(Ytest(j,:)`);
        [output, result] = max(Z{end});
        if expected==result
            numCorrect=numCorrect+1;
        end
    end 
    testAccuracy = numCorrect/Nxtest
    Accuracy = [Accuracy testAccuracy];

    testCSqErr= (testCSqErr) /(Nxtest);        % //Average error of N sample after an epoch 
    testMse=testCSqErr
    testErr = [testErr testMse];

    epoch  = epoch+1
    Epo = [Epo epoch];   


    if mse < Min_Error
    Min_Error=mse;
        Min_Error_Epoch=epoch;
    end

end
% -- End Training Epoch Loop ------
% ---------------------------------

%plot epoch versus error graph
hold off
plot (Epo,Err)  % plot based on full epoch (Blue Line)
hold on
plot(Epo, testErr) % Red Line
hold off
figure

minErr= min(Err);
minTestErr = min(testErr);
fileID = fopen(FILENAME,`w`);
fprintf(fileID,`Min Training MSE: %e\n`, minErr);
fprintf(fileID,`Min Test MSE: %e\n`,minTestErr);
fclose(fileID);
plot(Epo, Accuracy) %Figure 2 Blue Line

```